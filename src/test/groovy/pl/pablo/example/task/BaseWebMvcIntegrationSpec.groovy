package pl.pablo.example.task

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import spock.lang.Specification

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print

@ActiveProfiles("test")
class BaseWebMvcIntegrationSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    ResultActions doRequest(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        return mockMvc.perform(requestBuilder).andDo(print())
    }

}
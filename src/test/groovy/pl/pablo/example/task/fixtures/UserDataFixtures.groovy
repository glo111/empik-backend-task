package pl.pablo.example.task.fixtures

import pl.pablo.example.task.feature.user.model.UserData

class UserDataFixtures {

    static def someUserData(String login) {
        return UserData.builder()
                .id("id")
                .login(login)
                .name("name")
                .type("type")
                .avatarUrl("avatarUrl")
                .createdAt("createdAt")
                .followers(10)
                .publicRepos(10)
                .build()
    }
}
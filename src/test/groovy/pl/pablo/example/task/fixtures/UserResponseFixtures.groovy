package pl.pablo.example.task.fixtures

import pl.pablo.example.task.feature.user.model.UserResponse

class UserResponseFixtures {

    static def someUserResponse(String login) {
        return UserResponse.builder()
                .id("id")
                .login(login)
                .name("name")
                .type("type")
                .avatarUrl("avatarUrl")
                .createdAt("createdAt")
                .calculations("7.2000000000")
                .build()
    }
}

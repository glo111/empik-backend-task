package pl.pablo.example.task.feature.user

import com.fasterxml.jackson.databind.ObjectMapper
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import pl.pablo.example.task.BaseWebMvcIntegrationSpec
import pl.pablo.example.task.core.db.repository.RequestCountRepository
import pl.pablo.example.task.feature.user.controller.UserController
import pl.pablo.example.task.feature.user.service.GitHubUserService

import static pl.pablo.example.task.fixtures.UserDataFixtures.someUserData
import static pl.pablo.example.task.fixtures.UserResponseFixtures.someUserResponse

@AutoConfigureWebMvc
@WebMvcTest(controllers = [UserController])
@Import([DefaultUserProcessorImpl])
class UserControllerTest extends BaseWebMvcIntegrationSpec {

    @Autowired
    DefaultUserProcessorImpl userProcessor

    @SpringBean
    GitHubUserService gitHubUserService = Mock()

    @SpringBean
    RequestCountRepository requestCountRepository = Mock()


    def "Should return user details"() {
        given:
        def objectMapper = new ObjectMapper();
        def login = "zero"
        gitHubUserService.getUserData(login) >> Optional.of(someUserData(login))
        requestCountRepository.findByLogin(login) >> Optional.empty()

        when:
        def result = doRequest(MockMvcRequestBuilders.get("/users/${login}")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response

        then:
        result.status == HttpStatus.OK.value()
        result.contentAsString == objectMapper.writeValueAsString(someUserResponse(login))
    }

}
drop table if exists request_count;

create table request_count(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(50) NOT NULL,
    count INTEGER NOT NULL
);
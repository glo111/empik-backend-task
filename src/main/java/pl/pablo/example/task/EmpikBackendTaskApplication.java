package pl.pablo.example.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.pablo.example.task.core.configuration.params.UrlParams;

@SpringBootApplication
@EnableJpaRepositories("pl.pablo.example.task.core.db.repository")
@EnableConfigurationProperties({UrlParams.class})
public class EmpikBackendTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmpikBackendTaskApplication.class, args);
    }

}

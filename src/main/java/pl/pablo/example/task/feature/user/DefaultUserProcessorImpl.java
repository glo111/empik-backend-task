package pl.pablo.example.task.feature.user;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.pablo.example.task.core.db.repository.RequestCountRepository;
import pl.pablo.example.task.core.exceptions.GitHubUserServiceException;
import pl.pablo.example.task.feature.user.model.UserData;
import pl.pablo.example.task.feature.user.model.UserResponse;
import pl.pablo.example.task.feature.user.service.GitHubUserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import static java.util.Objects.isNull;

@Slf4j
@Component
@AllArgsConstructor
public class DefaultUserProcessorImpl implements UserProcessor {

    private static final BigDecimal FIRST_CALCULATION_CONDITION = new BigDecimal(6);
    private static final BigDecimal SECOND_CALCULATION_CONDITION = new BigDecimal(2);

    private GitHubUserService gitHubUserService;
    private RequestCountRepository requestCountRepository;

    @Override
    public UserResponse process(String login) {
        var userData = gitHubUserService.getUserData(login);
        var isPresent = requestCountRepository.findByLogin(login).isPresent();
        if (!isPresent) {
            requestCountRepository.insertNewUserInformation(login);
        }
        requestCountRepository.countApiCall(login);
        return prepareResponse(userData);
    }

    private UserResponse prepareResponse(Optional<UserData> userData) {
        return userData.map(data -> UserResponse.builder()
                        .id(data.getId())
                        .login(data.getLogin())
                        .name(data.getName())
                        .type(data.getType())
                        .avatarUrl(data.getAvatarUrl())
                        .createdAt(data.getCreatedAt())
                        .calculations(prepareCalculations(data))
                        .build())
                .orElseThrow(() -> new GitHubUserServiceException("Brak danych", "Nie znaleziono użytkownika z powodu problemów z połączeniem."));
    }

    private String prepareCalculations(UserData userData) {
        if (isNull(userData.getFollowers()) || userData.getFollowers() == 0) {
            return "Nie można dzielić przez 0";
        }

        if (isNull(userData.getPublicRepos())) {
            return "Zmienna public_repos jest nullem. Nie mogę dokonać obliczeń";
        }
        var firstCalculation = FIRST_CALCULATION_CONDITION.divide(new BigDecimal(userData.getFollowers()), 10, RoundingMode.HALF_UP);
        var secondCalculation = SECOND_CALCULATION_CONDITION.add(new BigDecimal(userData.getPublicRepos()));
        return (firstCalculation.multiply(secondCalculation)).toString();
    }
}

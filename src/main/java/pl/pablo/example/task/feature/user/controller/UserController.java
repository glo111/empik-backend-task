package pl.pablo.example.task.feature.user.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.pablo.example.task.core.exceptions.GitHubUserServiceException;
import pl.pablo.example.task.feature.user.UserProcessor;
import pl.pablo.example.task.feature.user.model.UserResponse;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
@Validated
@Tag(name = "User API", description = "API do pobierania użytkowników z GitHub")
public class UserController {

    private UserProcessor userProcessor;

    @Operation(
            summary = "Pobiera dane użytkownika GitHub",
            description = "Zwraca informacje o użytkowniku na podstawie loginu",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Sukces", content = @Content(schema = @Schema(implementation = UserResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Nieprawidłowy login"),
                    @ApiResponse(responseCode = "500", description = "Błąd serwera")
            }
    )
    @GetMapping("/users/{login}")
    public ResponseEntity<UserResponse> getUser(@PathVariable @NotBlank @NotNull String login) {
        log.info("Rozpoczynam procesowanie informacji o użytkowniku");
        return ResponseEntity.ok(userProcessor.process(login));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(GitHubUserServiceException.class)
    public Map<String, String> handelValidationException(GitHubUserServiceException e) {
        var errors = new HashMap<String, String>();
        errors.put(e.getMessage(), e.getReason());
        return errors;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map<String, String>> handleConstraintViolationException(ConstraintViolationException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getConstraintViolations().forEach(violation -> {
            errors.put(violation.getPropertyPath().toString(), violation.getMessage());
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(AssertionError.class)
    public Map<String, String> handelValidationException(AssertionError e) {
        var errors = new HashMap<String, String>();
        errors.put("Serwer nie zwrocił żadnych informacji", e.getMessage());
        return errors;
    }

}

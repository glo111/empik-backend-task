package pl.pablo.example.task.feature.user.service;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.pablo.example.task.core.api.github.GitHubClient;
import pl.pablo.example.task.core.exceptions.GitHubUserServiceException;
import pl.pablo.example.task.feature.user.model.UserData;

import java.io.IOException;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static pl.pablo.example.task.feature.user.converter.UserDataConverter.convert;

@Slf4j
@Service
@AllArgsConstructor
public class GitHubUserService {

    private GitHubClient gitHubClient;

    @CircuitBreaker(name = "githubClient", fallbackMethod = "fallback")
    @Retry(name = "githubClient", fallbackMethod = "fallback")
    public Optional<UserData> getUserData(String login) {
        try {
            var body = gitHubClient.getUser(login).execute().body();
            assert nonNull(body);
            return Optional.of(convert(body));
        } catch (IOException e) {
            throw new GitHubUserServiceException(e.getMessage(), "Wystapił problem podczas próby pobrania danych dla użytkownika" + login);
        }
    }

    public Optional<UserData> fallback(String login, Throwable t) {
        log.error("Nie można wydobyć danych z GitHubUserService. Login {}", login);
        return Optional.empty();
    }

}

package pl.pablo.example.task.feature.user;

import pl.pablo.example.task.feature.user.model.UserResponse;

public interface UserProcessor {

    UserResponse process(String login);

}

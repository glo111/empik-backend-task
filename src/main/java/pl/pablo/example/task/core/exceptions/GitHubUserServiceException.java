package pl.pablo.example.task.core.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GitHubUserServiceException extends Error {
    private String message;
    private String reason;
}

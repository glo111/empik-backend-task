package pl.pablo.example.task.core.configuration.params;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "url-data")
@Getter
@Setter
public class UrlParams {
    private String githubHost;
}

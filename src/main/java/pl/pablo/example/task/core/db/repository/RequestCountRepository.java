package pl.pablo.example.task.core.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.pablo.example.task.core.db.entity.RequestCountEntity;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface RequestCountRepository extends JpaRepository<RequestCountEntity, Long> {

    @Query(value = "select request_count from RequestCountEntity request_count where request_count.login = :login")
    Optional<RequestCountEntity> findByLogin(@Param("login") String login);

    @Modifying
    @Query(value = "update RequestCountEntity table set table.count = table.count + 1 where table.login = :login")
    @Transactional
    void countApiCall(@Param("login") String login);

    @Modifying
    @Query(value = "insert into request_count (login, count) values(:login, 0)", nativeQuery = true)
    @Transactional
    void insertNewUserInformation(@Param("login") String login);

}

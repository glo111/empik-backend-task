package pl.pablo.example.task.core.api.github;

import pl.pablo.example.task.feature.user.model.GitHubUserResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubClient {

    @GET("users/{name}")
    Call<GitHubUserResponse> getUser(@Path("name") String name);

}

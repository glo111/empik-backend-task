package pl.pablo.example.task.core.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.pablo.example.task.core.api.github.GitHubClient;
import pl.pablo.example.task.core.configuration.params.UrlParams;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
@AllArgsConstructor
public class GitHubClientConfig {

    private OkHttpClient okHttpClient;
    private ObjectMapper mapper;
    private UrlParams urlParams;

    @Bean
    public GitHubClient gitHubClient() {
        return new Retrofit.Builder()
                .baseUrl(urlParams.getGithubHost())
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build()
                .create(GitHubClient.class);
    }
}

package pl.pawelLipinski.example.task.feature.user.converter;

import pl.pawelLipinski.example.task.feature.user.model.GitHubUserResponse;
import pl.pawelLipinski.example.task.feature.user.model.UserData;

public class UserDataConverter {

    public static UserData convert(GitHubUserResponse gitHubUserResponse) {
        return UserData.builder()
                .id(gitHubUserResponse.getId())
                .login(gitHubUserResponse.getLogin())
                .name(gitHubUserResponse.getName())
                .type(gitHubUserResponse.getType())
                .avatarUrl(gitHubUserResponse.getAvatarUrl())
                .createdAt(gitHubUserResponse.getCreatedAt())
                .build();
    }

}

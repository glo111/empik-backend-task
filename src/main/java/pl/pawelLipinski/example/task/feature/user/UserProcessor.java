package pl.pawelLipinski.example.task.feature.user;

import pl.pawelLipinski.example.task.feature.user.model.UserResponse;

public interface UserProcessor {

    UserResponse process(String login);

}

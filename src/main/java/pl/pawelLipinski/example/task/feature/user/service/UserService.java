package pl.pawelLipinski.example.task.feature.user.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.pawelLipinski.example.task.common.exception.ConnectionProblemException;
import pl.pawelLipinski.example.task.common.exception.SpecifiedUserDoesNotExistException;
import pl.pawelLipinski.example.task.feature.user.model.GitHubUserResponse;
import pl.pawelLipinski.example.task.feature.user.model.UserData;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static pl.pawelLipinski.example.task.feature.user.converter.UserDataConverter.convert;

@Slf4j
@Service
public class UserService {

    private static final String api = "https://api.github.com/users/";
    private static final OkHttpClient client = new OkHttpClient()
            .newBuilder()
            .readTimeout(3, TimeUnit.SECONDS)
            .build();
    private static final ObjectMapper mapper = new ObjectMapper();


    public UserData getUserData(String login) {
        try {
            validate(login);

            var request = new Request.Builder()
                    .url(new StringBuilder()
                            .append(api)
                            .append(URLEncoder.encode(login, "Windows-1250"))
                            .toString())
                    .build();

            var response = client.newCall(request).execute();
            assert nonNull(response.body());
            var body = response.body().string();
            validate(response, login);
            return convert(mapper.readValue(body, GitHubUserResponse.class));
        } catch (IOException exception) {
            log.error(exception.getMessage());
            throw new ConnectionProblemException("Wystapił problem podczas próby pobrania danych dla użytkownika", login);
        }
    }

    private void validate(String login) {
        log.info("Zaczynam walidować login");
        if (isNull(login)) {
            throw new SpecifiedUserDoesNotExistException("Podany login jest nullem. Wprowadź prawidłową wartość", login);
        }
    }

    private void validate(Response response, String login) {
        log.info("Zaczynam walidować odpowiedz");
        switch (HttpStatus.valueOf(response.code())) {
            case OK:
                log.info("Odpowiedz jest prawidłowa");
                break;
            case NOT_FOUND:
                throw new SpecifiedUserDoesNotExistException("Podany użytkownik nie został znaleziony", login);
            case BAD_REQUEST:
                throw new SpecifiedUserDoesNotExistException("Wystąpił problem przetwarzania użytkownika", login);
            case INTERNAL_SERVER_ERROR:
                throw new ConnectionProblemException("Wystapił problem podczas przetwarzania danych dla uzytkownika", login);
        }
    }
}

package pl.pawelLipinski.example.task.feature.user.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pawelLipinski.example.task.common.exception.ConnectionProblemException;
import pl.pawelLipinski.example.task.common.exception.SpecifiedUserDoesNotExistException;
import pl.pawelLipinski.example.task.feature.user.UserProcessor;
import pl.pawelLipinski.example.task.feature.user.model.UserResponse;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
public class UserController {

    private UserProcessor userProcessor;

    @GetMapping("/users/{login}")
    public ResponseEntity<UserResponse> getUser(@PathVariable String login) {
        log.info("Rozpoczynam procesowanie informacji o użytkowniku");
        return ResponseEntity.ok(userProcessor.process(login));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConnectionProblemException.class)
    public Map<String, String> handelValidationException(ConnectionProblemException e) {
        var errors = new HashMap<String, String>();
        errors.put(e.getMessage(), e.getReason());
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SpecifiedUserDoesNotExistException.class)
    public Map<String, String> handelValidationException(SpecifiedUserDoesNotExistException e) {
        var errors = new HashMap<String, String>();
        errors.put(e.getMessage(), e.getUserName());
        return errors;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(AssertionError.class)
    public Map<String, String> handelValidationException(AssertionError e) {
        var errors = new HashMap<String, String>();
        errors.put("Serwer nie zwrocił żadnych informacji", e.getMessage());
        return errors;
    }

}

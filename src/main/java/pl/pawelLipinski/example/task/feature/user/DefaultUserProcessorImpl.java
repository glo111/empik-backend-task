package pl.pawelLipinski.example.task.feature.user;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.pawelLipinski.example.task.common.db.repository.RequestCountRepository;
import pl.pawelLipinski.example.task.feature.user.model.UserData;
import pl.pawelLipinski.example.task.feature.user.model.UserResponse;
import pl.pawelLipinski.example.task.feature.user.service.UserService;

import static java.util.Objects.isNull;

@Slf4j
@Component
@AllArgsConstructor
public class DefaultUserProcessorImpl implements UserProcessor {

    private static final int FIRST_CALCULATION_CONDITION = 6;
    private static final int SECOND_CALCULATION_CONDITION = 2;

    private UserService userService;
    private RequestCountRepository requestCountRepository;

    @Override
    public UserResponse process(String login) {
        var userData = userService.getUserData(login);
        var isPresent = requestCountRepository.findByLogin(login).isPresent();
        if (!isPresent) {
            requestCountRepository.insertNewUserInformation(login);
        }
        requestCountRepository.countApiCall(login);
        return prepareResponse(userData);
    }

    private UserResponse prepareResponse(UserData userData) {
        return UserResponse.builder()
                .id(userData.getId())
                .login(userData.getLogin())
                .name(userData.getName())
                .type(userData.getType())
                .avatarUrl(userData.getAvatarUrl())
                .createdAt(userData.getCreatedAt())
                .calculations(prepareCalculations(userData))
                .build();
    }

    private Integer prepareCalculations(UserData userData) {
        if (isNull(userData.getFollowers())) {
            return 0;
        } else if (isNull(userData.getPublicRepos())) {
            return FIRST_CALCULATION_CONDITION / userData.getFollowers() * (SECOND_CALCULATION_CONDITION);
        } else {
            return FIRST_CALCULATION_CONDITION / userData.getFollowers() * (SECOND_CALCULATION_CONDITION + userData.getPublicRepos());
        }
    }

}

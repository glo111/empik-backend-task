package pl.pawelLipinski.example.task.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SpecifiedUserDoesNotExistException extends Error {

    private String message;
    private String userName;

}

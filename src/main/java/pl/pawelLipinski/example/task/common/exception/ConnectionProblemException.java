package pl.pawelLipinski.example.task.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ConnectionProblemException extends Error {

    private String message;
    private String reason;

}
